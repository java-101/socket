'use strict';

let app = require('express')();
let http = require('http').Server(app);
let server = require('http').createServer(app);
let io = require('socket.io')(http);
const host = "localhost";
io.on('connection', (socket) => {
    socket.on('disconnect', function(){
        console.log('user disconnected');
    });

    socket.on('new',  (data) =>{
        socket.broadcast.emit('created', {obj: data, mode:'add'} );
    });

    socket.on('delete', (idx) => {
        socket.broadcast.emit('deleted', {id: idx});
    });

    socket.on('edit', (data)=>{
        console.log('data', data);
        io.emit('created', { obj: data, mode:'edit' })
    });


});

http.listen(5000, host , () => {
});
